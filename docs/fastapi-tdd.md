# FastAPI: 테스트 중심 개발의 예<sup>[1](#footnote_1)</sup>

다음은 FastAPI를 사용하여 테스트 주도 개발(TDD, Test Driven Development)을 적용하는 방법에 대한 간단한 예이다.

사전 요구 사항: FastAPI, Uvicorn(ASGI 서버), pytest를 설치한다.

```bash
$ pip install fastapi uvicorn pytest
```

바로 시작하자 🏃‍♂️

## Step 1: 실패 테스트 작성

먼저 ID로 책을 검색하는 엔드포인트 `/books/{id}`를 만들어 보겠다. 먼저, `test_main.py`에 실패 테스트를 작성한다.

```python
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

def test_read_book():
    response = client.get("/books/1")
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "title": "1984",
        "author": "George Orwell",
        "availability": True
    }
```

pytest로 테스트를 실행한다.

```bash
$ pytest
```

아직 엔드포인트를 구현하지 않았기 때문에 테스트는 실패한다.

## Step 2: 코드 구현

`main.py`에서 FastAPI 앱을 빌드해 보겠다.

```python
from fastapi import FastAPI

app = FastAPI()

@app.get("/books/{id}")
def read_book(id: int):
    pass
```

테스트를 다시 실행해도 엔드포인트가 예상 응답을 반환하지 않으므로 여전히 실패이다.

## Step 3: 테스트 통과하기

이제 엔드포인트에서 하드코딩된 책을 반환해 보겠다.

```python
@app.get("/books/{id}")
def read_book(id: int):
    return {
        "id": id,
        "title": "1984",
        "author": "George Orwell",
        "availability": True
    }
```

테스트를 다시 실행하면 이제 엔드포인트가 예상대로 작동하므로 테스트를 통과할 것이다.

## Step 4: 리팩터링

이 단계에서는 테스트가 여전히 통과하는지 확인하면서 코드를 리팩터링한다. `Book` 모델과 데이터베이스를 시뮬레이션하는 `FakeDB` 클래스를 만들었다고 가정한다.

```python
from pydantic import BaseModel
from typing import Dict

class Book(BaseModel):
    id: int
    title: str
    author: str
    availability: bool

class FakeDB:
    def __init__(self):
        self.data: Dict[int, Book] = {}
    def get_book(self, id: int) -> Book:
        return self.data.get(id)

db = FakeDB()
db.data = {
    1: Book(id=1, title="1984", author="George Orwell", availability=True)
}

@app.get("/books/{id}", response_model=Book)
def read_book(id: int):
    return db.get_book(id)
```

테스트를 다시 실행해도 여전히 통과할 수 있으며, 이제 더 현실적인(여전히 단순화되었지만) 어플리케이션을 만들 수 있습니다.


<a name="footnote_1">1</a>: 이 페이지는 [FastAPI: An Example of Test Driven Development](https://medium.com/@kasperjuunge/fastapi-an-example-of-test-driven-development-%EF%B8%8F-21109ea901ae)을 편역한 것임.
